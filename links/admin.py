from django.contrib import admin

from links.models import (
    LinkCategory,
    UserLinkCategory,
    Link
)


class LinkCategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'description',
    )

    search_fields = [
        'name',
        'description'
    ]

admin.site.register(LinkCategory, LinkCategoryAdmin)


class UserLinkCategoryAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'date_created'
    )

    search_fields = [
        'user',
        'parent_category'
    ]
admin.site.register(UserLinkCategory, UserLinkCategoryAdmin)


class LinkAdmin(admin.ModelAdmin):
    list_display = (
        'user_category',
        'title',
        'date_created'
    )

    search_fields = [
        'title',
        'user_category'
    ]
admin.site.register(Link, LinkAdmin)
