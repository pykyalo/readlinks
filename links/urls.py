from django.conf.urls import url, include

from .views import (
    LinkCategoryView,
    LinkView,
    LinkDetails,
    UserCategoryView,
    UserCategoryDetailsView,
    UserCategoryUpdateView
)

urlpatterns = [
    url(r'^link-categories', LinkCategoryView.as_view(), name='link-categories'),
    url(r'^links/$', LinkView.as_view(), name='links'),
    url(r'^links/(?P<pk>[0-9]+)/$',
        LinkDetails.as_view(),
        name='link-details'),
    url(r'^links/2/$',
        LinkDetails.as_view(),
        name='link-details'),
    url(r'^usercategories/$', UserCategoryView.as_view(), name='usercategories'),
    url(r'^usercategories/(?P<parent_category_id>[0-9]+)/$', UserCategoryDetailsView.as_view(), name='user-parent-categories-details'),
    url(r'^usercategory/(?P<pk>[0-9]+)/$', UserCategoryUpdateView.as_view(), name='update-custom-category'),
]
