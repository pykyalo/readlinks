from rest_framework.generics import ListAPIView, ListCreateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from django.http import Http404

from users.models import User
from .models import LinkCategory, Link, UserLinkCategory
from .serializers import (
    LinkCategorySerializer,
    LinkSerializer,
    MiniLinkSerializer,
    LeanUserLinkCategorySerializer,
    UserLinkCategorySerializer
)


class LinkCategoryView(ListAPIView):
    """
        DESCRIPTION | Returns a list of all categories created by readlinks admin \n
        GET | Takes no params
    """
    queryset = LinkCategory.objects.filter(is_available=True)
    serializer_class = LinkCategorySerializer

    def get(self, request, format=None):
        queryset = self.get_queryset()
        link_categories_serializer = self.serializer_class(queryset, many=True)
        return Response(link_categories_serializer.data, status=status.HTTP_200_OK)


class UserCategoryUpdateView(APIView):
    """
        DESCRIPTION: Allows users to update a custom category
    """

    permission_classes = (IsAuthenticated, )

    def put(self, request, pk, format=None):
        user_category_obj = UserLinkCategory.objects.get(
            id=pk
        )
        data = {
            "name": request.data["name"],
            "user": request.user.id,
            "parent_category": request.data["parent_category_id"]
        }

        custom_category_serializer = LeanUserLinkCategorySerializer(
            user_category_obj,
            data=data,
            partial=True
        )

        if custom_category_serializer.is_valid():
            custom_category_serializer.save()
            return Response(
                custom_category_serializer.data,
                status=status.HTTP_200_OK
            )

        return Response(
            custom_category_serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )



class UserCategoryDetailsView(APIView):
    """
        DESCRIPTION: Categories for a particular user filtered by the category
    """

    permission_classes = (IsAuthenticated, )

    def get(self, request, parent_category_id, format=None):
        user_category_queryset = UserLinkCategory.objects.filter(
            user_id=request.user.id,
            parent_category_id=parent_category_id
        )
        user_category_serializer = UserLinkCategorySerializer(
            user_category_queryset,
            many=True
        )

        return Response(user_category_serializer.data, status=status.HTTP_200_OK)

class UserCategoryView(APIView):
    """
        DESCRIPTION | Must be authenticated \n
        POST | Returns user link category obj\n
             {
                "category_id": 1,
                "names": ["My custom topic", "Another topic in this category"]
             }
        GET  | Returns all the categories created by a user
    """

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):
        user_category_queryset = UserLinkCategory.objects.filter(user_id=request.user.id)

        user_category_serializer = UserLinkCategorySerializer(
            user_category_queryset,
            many=True
        )

        return Response(user_category_serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        parent_category_id = request.data['category_id']
        names = request.data['names']
        user = User.objects.get(id=request.user.id)

        try:
            category_obj = LinkCategory.objects.get(id=parent_category_id)
        except:
            error = {
                'error': 'Category ID is invalid'
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        user_category_queryset = UserLinkCategory.objects.filter(
            user_id=user.id,
            parent_category_id=parent_category_id
        )

        already_created_names = []

        for user_category in user_category_queryset:
            already_created_names.append(user_category.name.lower())

        # Get all the names not in already_created_names created
        names_not_created = set(names) - set(already_created_names)
        new_names_only_list = list(names_not_created)

        for name in new_names_only_list:
            user_category = UserLinkCategory(
                parent_category=category_obj,
                name=name,
                user=user
            )
            user_category.save()

        new_user_category = UserLinkCategory.objects.filter(
            user_id=user.id,
            parent_category_id=parent_category_id
        )
        new_user_category_serializer = LeanUserLinkCategorySerializer(
            new_user_category,
            many=True
        )

        return Response(
            new_user_category_serializer.data, status=status.HTTP_201_CREATED
        )




class LinkView(APIView):

    """
        DESCRIPTION | Must be authenticated\n
        POST | Returns a link object\n
             {
                "owner": `<user_id>`,
                "category": `<category_id>`
                "title": "htps://medium.com/content/my-good-read/",
                "description": "This is sample description of my good read title"
             }
    """

    permission_classes = (IsAuthenticated, )
    serializer_class = LinkSerializer

    def get(self, request, format=None):
        user = request.user

        links = Link.objects.filter(owner_id=user.id, is_active=True)
        links_serializer = self.serializer_class(links, many=True)
        return Response(links_serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        user = request.user

        data = {
            "user_category": request.data["user_category_id"],
            "title": request.data["title"],
            "description": request.data["description"]
        }

        link_serializer = MiniLinkSerializer(data=data)

        if link_serializer.is_valid():
            link_serializer.save()
            return Response(link_serializer.data, status=status.HTTP_201_CREATED)

        return Response(link_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LinkDetails(APIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = MiniLinkSerializer

    def get_object(self, pk):
        try:
            link_obj = Link.objects.get(id=pk)
            return link_obj
        except:
            raise Http404()

    def get(self, request, pk, format=None):
        link = self.get_object(pk)

        if link:
            link_serializer = LinkSerializer(link)
            return Response(link_serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk, format=None):
        link = self.get_object(pk)
        user = request.user

        if link:
            data = {
                'owner': user.id,
                'title': request.data['title'],
                'category': request.data['category'],
                'description': request.data['description']
            }

            link_serializer = self.serializer_class(link, data=data, partial=True)

            if link_serializer.is_valid():
                link_serializer.save()
                return Response(link_serializer.data, status=status.HTTP_201_CREATED)

            return Response(link_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        link = self.get_object(pk)

        link.is_active = False
        link.save()

        return Response(status=status.HTTP_200_OK)
