from rest_framework import serializers

from .models import Link, LinkCategory, UserLinkCategory

from users.serializers import UserSerializer


class LinkCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = LinkCategory
        fields = ('id', 'name', 'description', 'is_available', 'date_created')


class LinkSerializer(serializers.ModelSerializer):

    category = LinkCategorySerializer()

    class Meta:
        model = Link
        fields = ('user_category', 'title', 'description', 'date_created', 'is_active')


class MiniLinkSerializer(serializers.ModelSerializer):
    # used on a post method
    class Meta:
        model = Link
        fields = ('user_category', 'title', 'description', 'date_created', 'is_active')


class UserLinkCategorySerializer(serializers.ModelSerializer):

    parent_category = LinkCategorySerializer()

    class Meta:
        model = UserLinkCategory
        fields =  ('user', 'name', 'parent_category', 'date_created')

class LeanUserLinkCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserLinkCategory
        fields =  ('user', 'name', 'parent_category', 'date_created')
