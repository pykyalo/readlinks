# Generated by Django 2.0.4 on 2018-09-04 06:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('links', '0010_auto_20180904_0611'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userlinkcategory',
            old_name='data_created',
            new_name='date_created',
        ),
        migrations.AlterField(
            model_name='userlinkcategory',
            name='category',
            field=models.ManyToManyField(related_name='categories', to='links.LinkCategory'),
        ),
    ]
