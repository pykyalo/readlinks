from django.db import models
from users.models import User


class LinkCategory(models.Model):

    """
        All links belong to a category e.g Programming, Python, Agile,
    """

    name = models.CharField(
        max_length=55
    )
    description = models.TextField(null=True, blank=True)
    is_available = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class UserLinkCategory(models.Model):
    """
        To have a personalized selection, a user creates there list of
        categories they are interested in from the main categories

        Tech | Gadgets
             | Algorithms
        Politics | Political Parties
                 | Kenyan Elections 2022
                 | Polictics in Ghana
        Programming | Python
                    | Algorithms
                    | Data structures
    """

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    parent_category = models.ForeignKey(
        LinkCategory, on_delete=models.CASCADE, null=True
    )
    name = models.CharField(
        max_length=100,
        null=True
    )
    date_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}".format(self.user.email)


class Link(models.Model):
    """
        A link is a url described by it's name and optional description
    """
    user_category = models.ForeignKey(
        'UserLinkCategory',
        on_delete=models.CASCADE,
        null=True
    )
    title = models.TextField()
    description = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
