# Generated by Django 2.0.4 on 2018-04-22 20:38

from django.db import migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='smsnotification',
            name='phonenumber',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=128),
        ),
    ]
