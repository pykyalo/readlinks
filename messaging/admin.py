from django.contrib import admin

from .models import SMSNotification


admin.site.register(SMSNotification)