from .models import SMSNotification
from .AfricasTalkingGateway import AfricasTalkingGateway, AfricasTalkingGatewayException


def send_sms(body, phonenumber):
    username = 'pykyalo'
    apikey = 'b87b29ceb7eea5756acb00cce4eb354d870c7aa5dc8ea77774cd1857840eeb2b'
    to = "%s," % (phonenumber)

    message = body

    gateway = AfricasTalkingGateway(username, apikey)

    try:
        results = gateway.sendMessage(to, message)

        # save the message sent
        obj = SMSNotification.objects.create(
            phonenumber=phonenumber,
            body=body,
            category='SMS'
        )
        obj.save()
        return "SMS sent"
    except AfricasTalkingGatewayException as e:
        error = {
            "error_at": str(e)
        }
        return error
