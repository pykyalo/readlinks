from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class SMSNotification(models.Model):
    category = models.CharField(max_length=55)
    body = models.TextField()
    phonenumber = PhoneNumberField()
    date_sent = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.phonenumber)

