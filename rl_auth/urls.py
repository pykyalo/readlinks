from django.conf.urls import url, include


from .views import Login

urlpatterns = [
    url(r'^login', Login.as_view(), name='login'),
]