import random


def generate_token():
    """
        generates base64 tokens based on the the random string
    """

    first_three = random.randint(100, 999)
    last_three = random.randint(100, 999)

    raw_token = str(first_three) + " " + str(last_three)
    return raw_token
