from django.apps import AppConfig


class RlAuthConfig(AppConfig):
    name = 'rl_auth'
