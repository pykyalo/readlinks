from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class LoginToken(models.Model):
    hashed_token = models.TextField()
    phonenumber = PhoneNumberField(null=True)
    status = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.hashed_token