from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.authtoken.models import Token
import base64

from .utils import generate_token
from messaging.utils import send_sms
from .models import LoginToken
from users.models import User


class Login(APIView):
    """
        To get a token:
        {
            "phonenumber": "+254724230064"
        }

        To verify a token:

        {
            "phonenumber": "+254724230064",
            "login_token": "111 222"
        }
    """

    def post(self, request, format=None):
        phonenumber = request.data['phonenumber']

        # check if phonenumber exists
        if User.objects.filter(phonenumber=phonenumber).exists() is False:
            error = {
                "error": "Phonenumber is not registered"
            }
            return Response(error, status=status.HTTP_404_NOT_FOUND)

        # if login_token is provided, compare and get token

        try:
            login_token = request.data['login_token']
            byte_login_token = login_token.encode("utf-8")
            hashed_token = base64.b64encode(byte_login_token)

            token_obj = LoginToken.objects.filter(hashed_token=hashed_token, phonenumber=phonenumber, status=False).first()

            if token_obj:

                # update the token status
                token_obj.status = True

                token_obj.save()

                # get token
                user = User.objects.get(phonenumber=phonenumber)

                auth_token = Token.objects.get(user_id=user.id)

                content = {
                    "token": auth_token.key,
                    "phonenumber": phonenumber,
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "email": user.email
                }
                return Response(content, status=status.HTTP_200_OK)
            error = {"error": "Wrong token"}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        except:
            token = generate_token()
            token = token.encode("utf-8")

            # the token is saved in base64 format

            hashed_token = base64.b64encode(token)

            if LoginToken.objects.filter(hashed_token=hashed_token).exists():
                hashed_token = base64.b64encode(token)

            l_token = LoginToken.objects.create(hashed_token=hashed_token, phonenumber=phonenumber)
            l_token.save()

            if phonenumber:
                body = "Readlinks verification code is: {}".format(token.decode("utf-8"))

                send_sms(body, phonenumber)
                return Response(status=status.HTTP_201_CREATED)

            error = {"error": "Please provide a phonenumber"}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
