"""
    This contains root view for the apps apis
"""

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.reverse import reverse



class RootView(APIView):
    def get(self, request, format=None):
        return Response({
            "users": reverse('users', request=request),
            "user details": reverse('user-details', request=request),
            "login": reverse('login', request=request),
            "link categories": reverse('link-categories', request=request),
            "links": reverse('links', request=request),
            "link details": reverse("link-details", request=request),
            "usercategories": reverse("usercategories", request=request),
        })
