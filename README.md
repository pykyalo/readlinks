# readlinks
If anyone reads alot, many are the times they want to bookmark links for future reading. In most cases, someone pins the page on the browser or add it to the browser bookmarking feature.


However, many readers end up with lots of pinned pages and bookmarks and now sure way to track ones reading activities. This project is a simple tool that achieves the following:


	- Creating personal accounts
	- Tracking links one has saved and if read
	- Reporting on reading patterns 
	- Reminding users of untouched links 
	- Linking with content providers {medium api, flipboard api, etc}
	- searching links
	- links CRUD
	- links grouping in categories

Future plans {
	Have a browser plugin that picks HIGHLIGHTED links and save automatically
}
