from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns


from .views import UsersView, UserDetails
from api_root.views import RootView
from rl_auth.urls import urlpatterns as rl_paths
from links.urls import urlpatterns as links_paths


urlpatterns = [
    url(r'^$', RootView.as_view(), name='root_view'),
    url(r'^users/$', UsersView.as_view(), name='users'),
    url(r'^users/(?P<pk>[0-9]+)/$',
        UserDetails.as_view(),
        name='user-details'),
    url(r'^users/4/$',
        UserDetails.as_view(),
        name='user-details'),
] + rl_paths


urlpatterns = urlpatterns + links_paths