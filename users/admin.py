from django.contrib import admin
from django.contrib.auth.models import Group
from users.models import User, Country



admin.site.register(User)
admin.site.unregister(Group)

class CountryAdmin(admin.ModelAdmin):
    search_fields = ['name', 'code']
    list_display = ('name', 'code')

admin.site.register(Country, CountryAdmin)
