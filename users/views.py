from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token
from django.http import Http404
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from .serializers import UserSerializer, FullUserSerializer
from .models import User, Country
from .pagination import UsersPagination

class UsersView(ListCreateAPIView):
    pagination_class = UsersPagination
    serializer_class = UserSerializer
    queryset = User.objects.filter(is_active=True)

    def post(self, request, format=None):
        data = {
            'phonenumber': request.data['phonenumber'],
            'email': request.data['email'],
            'first_name': request.data['first_name'],
            'last_name': request.data['last_name']
        }
        user_serializer = self.serializer_class(data=data)

        if user_serializer.is_valid():
            user_serializer.save()
            return Response(user_serializer.data, status=status.HTTP_201_CREATED)
        return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def get(self, request, format=None):
        queryset = self.get_queryset()
        users_serializer = FullUserSerializer(queryset, many=True)

        return Response(users_serializer.data, status=status.HTTP_200_OK)



class UserDetails(APIView):
    """
        Updates user model
        fetches user details
        inactivates user
    """

    serializer_class = UserSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )

    def get_object(self, pk):
        try:
            user = User.objects.get(id=pk)
            return user
        except:
            raise Http404()

    def get(self, request, pk, format=None):
        user = self.get_object(pk)

        if user:
            user_serializer = FullUserSerializer(user)
            return Response(user_serializer.data, status=status.HTTP_200_OK)


    def put(self, request, pk, format=None):
        user = self.get_object(pk)

        data = {
            'first_name': request.data['first_name'],
            'last_name': request.data['last_name'],
            'email': request.data['email'],
            'phonenumber': request.data['phonenumber']
        }

        user_serializer = self.serializer_class(user, data=data, partial=True)

        if user_serializer.is_valid():
            user_serializer.save()
            return Response(user_serializer.data, status=status.HTTP_201_CREATED)
        return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):

        # make user inactive instead of deleting permanently.

        user = self.get_object(pk)

        if user:
            user.is_active = False
            user.save()
            return Response(status=status.HTTP_200_OK)
