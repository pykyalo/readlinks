from users.models import User, Country
from rest_framework import serializers


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = (
            'id',
            'code',
            'name',
            'date_created'
        )
        read_only_fields = ('id', )


class FullUserSerializer(serializers.ModelSerializer):

    country = CountrySerializer()

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'phonenumber',
            'email',
            'country',
            'id'
        )


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'phonenumber',
            'email',
            'country',
            'id'
        )
        read_only_fields = ('id', )


    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            phonenumber=validated_data['phonenumber'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        user.save()
        return user
