from django.db import models
from django.contrib.auth.models import (
    BaseUserManager,
    AbstractBaseUser
)
from phonenumber_field.modelfields import PhoneNumberField


class UserManager(BaseUserManager):
    def create_user(self, phonenumber, email, first_name=None, last_name=None):
        """
            Creates user with email and optional phonenumber
        """

        if not email:
            raise ValueError('User Must Have an Email')

        if not phonenumber:
            raise ValueError('User Must Have a Phonenumber')

        user = self.model(
            phonenumber=phonenumber,
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name
        )

        user.save(using=self._db)
        return user



    def create_superuser(self, email, password, phonenumber=None):
        """
            Creates super user
        """
        if not email:
            raise ValueError('Admin Must provide a valid password')

        if not password:
            raise ValueError('Admin Must Provide a Password')

        user = self.model(
            email=self.normalize_email(email),
            phonenumber=phonenumber,
            password=password
        )

        user.set_password(password)
        user.is_admin = True
        user.save(using=self._db)
        return user


class Country(models.Model):
    name = models.CharField(
        max_length=30,
        unique=True
    )
    code = models.CharField(
        max_length=5,
        unique=True
    )
    date_created = models.DateTimeField(
        auto_now=True
    )

    class Meta:
        verbose_name_plural = 'Countries'

    def __str__(self):
        return "{}".format(self.name)


class User(AbstractBaseUser):
    first_name = models.CharField(
        max_length=55,
        null=True,
        blank=True
    )

    last_name = models.CharField(
        max_length=55,
        null=True,
        blank=True
    )

    email = models.EmailField(
        verbose_name='Email Address',
        unique=True,
        max_length=255
    )
    country = models.ForeignKey(
        Country,
        null=True,
        on_delete=models.CASCADE
    )
    phonenumber = PhoneNumberField(unique=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phonenumber']

    def __str__(self):
        return self.email


    def has_perm(self, perm, obj=None):
        return True


    def has_module_perms(self, app_label):
        return True


    @property
    def is_staff(self):
        return self.is_admin
